const assert = require('assert');

describe('Unit1 Case1', function () {
    describe('Example #indexOf() Array', function () {
        it('should return 0 when the value is in present', function () {
            assert.equal([1, 2, 3].indexOf(1), 0);
        });
    });
});