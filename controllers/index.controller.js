const handle = require('../utilitys/handle');

exports.hello = function (req, res) {
    res.send('hello api');
};

exports.test = function (req, res) {
    if (!req.body) {
        res.status(404).json(handle.error('data not found!'));
    } else {
        res.json(handle.success(req.body));
    }
};